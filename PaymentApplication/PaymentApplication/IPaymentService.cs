﻿namespace PaymentApplication
{
    public interface IPaymentService
    {
        bool ProcessPayment(CreditCard creditCard);
       
    }
}