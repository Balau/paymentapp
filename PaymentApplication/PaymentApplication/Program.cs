﻿using System;
using System.Collections.Generic;

namespace PaymentApplication
{
    public class Program
    {
        private static void Main()
        {
            CreditCard creditCard = null;

            try
            {
                ProcessPayment(creditCard);
            }
            catch (InvalidCreditCardException e)
            {
                DisplayErrorMessageInConsole(e.ErrorMessages());
            }
        }

        public static void ProcessPayment(CreditCard creditCard)
        {
            var creditCardValidator = new CreditCardValidator(creditCard);
            creditCardValidator.Validate();

            Process(creditCard);
        }

        private static void Process(CreditCard creditCard)
        {
            if (creditCard.Amount < 21)
            {
                ProcessCheapPayment(creditCard);
            }

            if (creditCard.Amount >= 21 && creditCard.Amount < 500)
            {
                ProcessExpensivePayment(creditCard);
            }

            if (creditCard.Amount >= 500)
            {
                ProcessPremiumPayment(creditCard);
            }
           
        }

        private static void ProcessCheapPayment(CreditCard creditCard)
        {
            IPaymentService paymentService = new CheapPaymentService();

            paymentService.ProcessPayment(creditCard);
        }

        private static void ProcessExpensivePayment(CreditCard creditCard)
        {
            IPaymentService paymentService = new ExpensivePaymentService();

            bool paymentIsProcessed = paymentService.ProcessPayment(creditCard);

            if (!paymentIsProcessed)
            {
                ProcessCheapPayment(creditCard);
            }
        }

        private static void ProcessPremiumPayment(CreditCard creditCard)
        {
            IPaymentService paymentService = new PremiumPaymentService();

            bool paymentIsProcessed = paymentService.ProcessPayment(creditCard);

            int retryCount = 0;
            while (!paymentIsProcessed && retryCount < 3)
            {
                paymentIsProcessed = paymentService.ProcessPayment(creditCard);
                retryCount++;
            }
        }

        private static void DisplayErrorMessageInConsole(IEnumerable<string> errorMessages)
        {
            foreach (var errorMessage in errorMessages)
            {
                Console.WriteLine(errorMessage);
            }
        }
    }
}
