﻿using System;

namespace PaymentApplication
{
    internal class PremiumPaymentService : IPaymentService
    {
        public bool ProcessPayment(CreditCard creditCard)
        {
            Console.WriteLine("Premium Payment has been processed successfully");
            return true;
        }
    }
}
