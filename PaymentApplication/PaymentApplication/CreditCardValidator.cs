﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PaymentApplication
{
    internal class CreditCardValidator
    {
        private readonly CreditCard _creditCard;
        private List<string> errorMessages= new List<string>();

        public CreditCardValidator(CreditCard creditCard)
        {
            _creditCard = creditCard;
        }

        internal void Validate()
        {
            ValidateCreditCardNumber(_creditCard);
            ValidateCardHolder(_creditCard);
            ValidateExpirationDate(_creditCard);
            ValidateSecurityCode(_creditCard);
            ValidateAmount(_creditCard);

            if (HasErrorMessages())
            {
                throw new InvalidCreditCardException(errorMessages);
            }
        }

        public bool HasErrorMessages()
        {
            return errorMessages.Any();
        }

        private void ValidateAmount(CreditCard creditCard)
        {
            if (creditCard.Amount < 0)
            {
                errorMessages.Add("Amount must be positive.");
            }
        }

        private void ValidateSecurityCode(CreditCard creditCard)
        {
            if (creditCard.SecurityCode < 100 || creditCard.SecurityCode > 999)
            {
                errorMessages.Add("Credit card security must have 3 digits.");
            }
        }

        private void ValidateExpirationDate(CreditCard creditCard)
        {
            if (creditCard.ExpirationDate < DateTime.Today)
            {
                errorMessages.Add("Expiration date cannot be before today.");
            }
        }

        private void ValidateCardHolder(CreditCard creditCard)
        {
            if (string.IsNullOrWhiteSpace(creditCard.CardHolder))
            {
                errorMessages.Add("Card holder cannot be null or empty.");
            }
        }

        private void ValidateCreditCardNumber(CreditCard creditCard)
        {
            if (string.IsNullOrWhiteSpace(creditCard.CreditCardNumber) || creditCard.CreditCardNumber.Length != 16)
            {
                errorMessages.Add("CreditCardNumber should contains 16 digits");
            }
        }

       
    }
}
