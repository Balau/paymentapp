﻿using System;

namespace PaymentApplication
{
    internal class CheapPaymentService : IPaymentService
    {
        public bool ProcessPayment(CreditCard creditCard)
        {
             Console.WriteLine("Cheap Payment has been processed successfully");
             return true;
        }
    }
}
