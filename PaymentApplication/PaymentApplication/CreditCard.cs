﻿using System;

namespace PaymentApplication
{
    public class CreditCard
    {
        public int SecurityCode { get; }
        public decimal Amount { get; }
        public string CreditCardNumber { get; }
        public string CardHolder { get; }
        public DateTime ExpirationDate { get; }

        public CreditCard(string creditCardNumber, string cardHolder, DateTime expirationDate, int securityCode, decimal amount)
        {
            CreditCardNumber = creditCardNumber;
            CardHolder = cardHolder;
            ExpirationDate = expirationDate;
            SecurityCode = securityCode;
            Amount = amount;
        }
       
    }
}
