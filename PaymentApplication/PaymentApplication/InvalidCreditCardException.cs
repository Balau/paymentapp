﻿using System;
using System.Collections.Generic;

namespace PaymentApplication
{
    public class InvalidCreditCardException : Exception
    {
        private readonly List<string> _errorMessages;

        public InvalidCreditCardException(List<string> errorMessages)
        {
            _errorMessages = errorMessages;
        }

        public IEnumerable<string> ErrorMessages()
        {
            return _errorMessages;
        }
    }
}