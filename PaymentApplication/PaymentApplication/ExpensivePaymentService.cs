﻿using System;

namespace PaymentApplication
{
    internal class ExpensivePaymentService : IPaymentService
    {
        public bool ProcessPayment(CreditCard creditCard)
        {
            Console.WriteLine("Expensive Payment has been processed successfully");
            return true;
        }
    }
}
